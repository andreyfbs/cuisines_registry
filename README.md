# Cuisines Registry

## The story:

Cuisines Registry is an important part of Book-That-Table Inc. backend stack. It keeps in memory customer preferences for restaurant cuisines and is accessed by a bunch of components to register and retrieve data. 


The first iteration of this component was implemented by rather inexperienced developer and now may require some cleaning while new functionality is being added. But fortunately, according to his words: "Everything should work and please keep the test coverage as high as I did"


## Your tasks:
1. **[Important!]** Adhere to the boy scout rule. Leave your code better than you found it.
It is ok to change any code as long as the CuisinesRegistry interface remains unchanged.
2. Make is possible for customers to follow more than one cuisine (return multiple cuisines in de.quandoo.recruitment.registry.api.CuisinesRegistry#customerCuisines)
3. Implement de.quandoo.recruitment.registry.api.CuisinesRegistry#topCuisines - returning list of most popular (highest number of registered customers) ones
4. Create a short write up on how you would plan to scale this component to be able process in-memory billions of customers and millions of cuisines (Book-That-Table is already planning for galactic scale). (100 words max)

## Submitting your solution

+ Fork it to a private gitlab repository.
+ Put write up mentioned in point 4. into this file.
+ Send us a link to the repository. together with private ssh key that allows access (settings > repository > deploy keys - **you need to generate a new key pair just for this**). **Please be careful about this** as it saves us a lot of time if done properly - We need an **unencrypted private key**; the public one you set in gitlab deploy keys settings.

# Resolution Item 4

1) To reach an horizontal scaling in memory (to scale vertically billions is not possible it would too much memory) we can think in a system that the keys of the Customers and the Cuisines can be distributed by range in multiple applications, for example:

One Application like a Gateway / Router to control the keys

Customers (between : 0 - 10000) to associate Cuisines Application A - Instance 1
Customers (between : 10000 - 20000) to associate Cuisines Application A - Instance 2
...

Cuisines (between : 0 - 10000) to associate Customers Application B - Instance 1
Cuisines (between : 10000 - 20000) to  associate Customers Application B - Instance 2
...

The challenge would be the topCuisines, maybe in this case keep a map of Millions in single instance can be possible, but it would still to consume a lot of memory.


2) Another approach is of course to use some in-memory data storage, like Memcached or Redis. They are already to prepare to scale horizontally in cluster.