package de.quandoo.recruitment.registry;

import com.google.common.collect.ImmutableList;
import de.quandoo.recruitment.registry.api.CuisinesRegistry;
import de.quandoo.recruitment.registry.model.Cuisine;
import de.quandoo.recruitment.registry.model.Customer;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

public class InMemoryCuisinesRegistryTest {

    private CuisinesRegistry cuisinesRegistry = new InMemoryCuisinesRegistry();

    @Test
    public void cuisineCustomersOneRecordWithMatch() {

        // Arrange
        final Cuisine cuisineParam = new Cuisine("french");

        final List<Customer> listExpected = Arrays.asList(new Customer("1"));

        cuisinesRegistry.register(new Customer("1"), new Cuisine("french"));

        // Act
        final List<Customer> listReturn = cuisinesRegistry.cuisineCustomers(cuisineParam);

        // Assert
        Assert.assertEquals(listExpected, listReturn);
    }

    @Test
    public void cuisineCustomersOneRecordWithoutMatch() {

        // Arrange
        final Cuisine cuisineParam = new Cuisine("portuguese");

        final List<Customer> listExpected = ImmutableList.of();

        cuisinesRegistry.register(new Customer("1"), new Cuisine("french"));

        // Act
        final List<Customer> listReturn = cuisinesRegistry.cuisineCustomers(cuisineParam);

        // Assert
        Assert.assertEquals(listExpected, listReturn);
    }


    @Test
    public void cuisineCustomersManyRecords() {

        // Arrange
        final Cuisine cuisineParam = new Cuisine("french");

        final List<Customer> listExpected = Arrays.asList(new Customer("1"), new Customer("4"));

        cuisinesRegistry.register(new Customer("1"), new Cuisine("french"));
        cuisinesRegistry.register(new Customer("2"), new Cuisine("german"));
        cuisinesRegistry.register(new Customer("3"), new Cuisine("italian"));
        cuisinesRegistry.register(new Customer("4"), new Cuisine("french"));

        // Act
        final List<Customer> listReturn = cuisinesRegistry.cuisineCustomers(cuisineParam);

        // Assert
        Assert.assertEquals(listExpected, listReturn);
    }


    @Test
    public void cuisineCustomersRepeatedElements() {

        // Arrange
        final Cuisine cuisineParam = new Cuisine("french");

        final List<Customer> listExpected = Arrays.asList(new Customer("1"), new Customer("4"));

        cuisinesRegistry.register(new Customer("1"), new Cuisine("french"));
        cuisinesRegistry.register(new Customer("2"), new Cuisine("german"));
        cuisinesRegistry.register(new Customer("2"), new Cuisine("german"));
        cuisinesRegistry.register(new Customer("1"), new Cuisine("french"));
        cuisinesRegistry.register(new Customer("4"), new Cuisine("french"));

        // Act
        final List<Customer> listReturn = cuisinesRegistry.cuisineCustomers(cuisineParam);

        // Assert
        Assert.assertEquals(listExpected, listReturn);
    }

    @Test
    public void customerCuisinesOneRecordWithMatch() {

        // Arrange
        final Customer customerParam = new Customer("1");

        final List<Cuisine> listExpected = Arrays.asList(new Cuisine("french"));

        cuisinesRegistry.register(new Customer("1"), new Cuisine("french"));

        // Act
        final List<Cuisine> listReturn = cuisinesRegistry.customerCuisines(customerParam);

        // Assert
        Assert.assertEquals(listExpected, listReturn);
    }

    @Test
    public void customerCuisinesOneRecordWithoutMatch() {

        // Arrange
        final Customer customerParam = new Customer("1");

        final List<Cuisine> listExpected = ImmutableList.of();

        cuisinesRegistry.register(new Customer("2"), new Cuisine("french"));

        // Act
        final List<Cuisine> listReturn = cuisinesRegistry.customerCuisines(customerParam);

        // Assert
        Assert.assertEquals(listExpected, listReturn);
    }


    @Test
    public void customerCuisinesManyRecords() {

        // Arrange
        final Customer customerParam = new Customer("1");

        final List<Cuisine> listExpected = Arrays.asList(new Cuisine("french"), new Cuisine("spanish"));

        cuisinesRegistry.register(new Customer("1"), new Cuisine("french"));
        cuisinesRegistry.register(new Customer("2"), new Cuisine("german"));
        cuisinesRegistry.register(new Customer("3"), new Cuisine("italian"));
        cuisinesRegistry.register(new Customer("1"), new Cuisine("spanish"));

        // Act
        final List<Cuisine> listReturn = cuisinesRegistry.customerCuisines(customerParam);

        // Assert
        Assert.assertEquals(listExpected, listReturn);
    }


    @Test
    public void customerCuisinesRepeatedElements() {

        // Arrange
        final Customer customerParam = new Customer("1");

        final List<Cuisine> listExpected = Arrays.asList(new Cuisine("french"), new Cuisine("spanish"));

        cuisinesRegistry.register(new Customer("1"), new Cuisine("french"));
        cuisinesRegistry.register(new Customer("2"), new Cuisine("german"));
        cuisinesRegistry.register(new Customer("3"), new Cuisine("italian"));
        cuisinesRegistry.register(new Customer("1"), new Cuisine("spanish"));

        // Act
        final List<Cuisine> listReturn = cuisinesRegistry.customerCuisines(customerParam);

        // Assert
        Assert.assertEquals(listExpected, listReturn);
    }

    @Test
    public void topCuisinesOneRecord() {

        // Arrange
        final int numberOfCuisine = 1;

        final List<Cuisine> listExpected = Arrays.asList(new Cuisine("french"));

        cuisinesRegistry.register(new Customer("1"), new Cuisine("french"));

        // Act
        List<Cuisine> listReturn = cuisinesRegistry.topCuisines(numberOfCuisine);

        // Assert
        Assert.assertEquals(listExpected, listReturn);
    }

    @Test
    public void topCuisinesEmptyRecords() {

        // Arrange
        final int numberOfCuisine = 1;

        final List<Cuisine> listExpected =  ImmutableList.of();

        // Act
        List<Cuisine> listReturn = cuisinesRegistry.topCuisines(numberOfCuisine);

        // Assert
        Assert.assertEquals(listExpected, listReturn);
    }

    @Test
    public void topCuisinesManyRecords() {

        // Arrange
        final int numberOfCuisine = 2;

        final List<Cuisine> listExpected = Arrays.asList(new Cuisine("french"), new Cuisine("italian"));

        // 3-french, 2-italian, 1-german
        cuisinesRegistry.register(new Customer("2"), new Cuisine("german"));
        cuisinesRegistry.register(new Customer("4"), new Cuisine("italian"));
        cuisinesRegistry.register(new Customer("4"), new Cuisine("french"));
        cuisinesRegistry.register(new Customer("7"), new Cuisine("french"));
        cuisinesRegistry.register(new Customer("9"), new Cuisine("italian"));
        cuisinesRegistry.register(new Customer("1"), new Cuisine("french"));

        // Act
        List<Cuisine> listReturn = cuisinesRegistry.topCuisines(numberOfCuisine);

        // Assert
        Assert.assertEquals(listExpected, listReturn);
    }

    @Test
    public void topCuisinesNumberGreaterThanRecords() {

        // Arrange
        final int numberOfCuisine = 100;

        final List<Cuisine> listExpected = Arrays.asList(new Cuisine("french"));

        cuisinesRegistry.register(new Customer("1"), new Cuisine("french"));

        // Act
        List<Cuisine> listReturn = cuisinesRegistry.topCuisines(numberOfCuisine);

        // Assert
        Assert.assertEquals(listExpected, listReturn);
    }

}