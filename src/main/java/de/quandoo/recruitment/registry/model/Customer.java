package de.quandoo.recruitment.registry.model;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;

public class Customer {

    private final String uuid;

    public Customer(final String uuid) {
        this.uuid = uuid;
    }

    public String getUuid() {
        return uuid;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Customer customer = (Customer) o;
        return Objects.equal(uuid, customer.uuid);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(uuid);
    }


    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("uuid", uuid)
                .toString();
    }

}
