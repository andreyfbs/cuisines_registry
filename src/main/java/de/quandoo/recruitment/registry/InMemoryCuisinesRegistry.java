package de.quandoo.recruitment.registry;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Sets;
import de.quandoo.recruitment.registry.api.CuisinesRegistry;
import de.quandoo.recruitment.registry.model.Cuisine;
import de.quandoo.recruitment.registry.model.Customer;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

public class InMemoryCuisinesRegistry implements CuisinesRegistry {

    private Map<Customer, Set<Cuisine>> cuisinesByCustomerMap = new ConcurrentHashMap<>();

    private Map<Cuisine, Set<Customer>> customersByCuisineMap = new ConcurrentHashMap<>();

    private Map<Integer, Set<Cuisine>> topCuisinesMap = new TreeMap<>(Collections.reverseOrder());

    @Override
    public void register(final Customer customer, final Cuisine cuisine) {

        // Check parameters
        if (customer == null || cuisine == null) {
            return;
        }

        // New objects to be persisted. Avoid changes by the Client
        final Customer customerEntity = new Customer(customer.getUuid());
        final Cuisine cuisineEntity = new Cuisine(cuisine.getName());

        // Map Custumer - Cuisines
        registerCuisinesByCustomerMap(customerEntity, cuisineEntity);


        // Map Cuisine - Customers
        registerCustomersbyCuisineMap(customerEntity, cuisineEntity);

        // Top Cuisines
        registerTopCuisines(cuisineEntity);

    }

    private void registerTopCuisines(Cuisine cuisineEntity) {

        // Initial Value
        int numberCustomers = 1;
        if (customersByCuisineMap.containsKey(cuisineEntity)) {
            numberCustomers = customersByCuisineMap.get(cuisineEntity).size();
        }

        // Include // Increment cuisine

        // Check if this key is already in the count Map
        if (topCuisinesMap.containsKey(numberCustomers)) {
            topCuisinesMap.get(numberCustomers).add(cuisineEntity);
        } else {
            topCuisinesMap.put(numberCustomers, Sets.newHashSet(cuisineEntity));
        }

        // Delete // Decrement cuisine
        // If the entry is already in the System
        if (numberCustomers > 1) {
            if (topCuisinesMap.containsKey(numberCustomers - 1)) {
                topCuisinesMap.get(numberCustomers - 1).remove(cuisineEntity);
            }
        }
    }

    private void registerCustomersbyCuisineMap(Customer customerEntity, Cuisine cuisineEntity) {
        if (customersByCuisineMap.containsKey(cuisineEntity)) {
            customersByCuisineMap.get(cuisineEntity).add(customerEntity);
        } else {
            customersByCuisineMap.put(cuisineEntity, Sets.newHashSet(customerEntity));
        }
    }

    private void registerCuisinesByCustomerMap(Customer customerEntity, Cuisine cuisineEntity) {
        if (cuisinesByCustomerMap.containsKey(customerEntity)) {
            cuisinesByCustomerMap.get(customerEntity).add(cuisineEntity);
        } else {
            cuisinesByCustomerMap.put(customerEntity, Sets.newHashSet(cuisineEntity));
        }
    }

    @Override
    public List<Customer> cuisineCustomers(final Cuisine cuisine) {
        if (cuisine != null && customersByCuisineMap.containsKey(cuisine)) {
            // Send a copy to the client
            return ImmutableList.copyOf(customersByCuisineMap.get(cuisine));

        } else {
            return ImmutableList.of();
        }
    }

    @Override
    public List<Cuisine> customerCuisines(final Customer customer) {
        if (customer != null && cuisinesByCustomerMap.containsKey(customer)) {
            // Send a copy to the client
            return ImmutableList.copyOf(cuisinesByCustomerMap.get(customer));

        } else {
            return ImmutableList.of();
        }
    }

    @Override
    public List<Cuisine> topCuisines(final int n) {
        List<Cuisine> cuisineList = topCuisinesMap.values().stream()
                .flatMap(Collection::stream)
                .limit(n)
                .collect(Collectors.toList());
        return cuisineList;
    }
}
